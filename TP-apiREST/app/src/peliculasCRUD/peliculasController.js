const { codigoOK, error500, error404 } = require('../utils/utils');
const Pelicula = require("../models/Pelicula");

module.exports.agregarPelicula = (req, res) => {

    const { _id ,nombre, anioEstreno, directores, elenco, comentarios } = req.body;

    const pelicula = new Pelicula({
        _id,
        nombre,
        anioEstreno,
        directores,
        elenco,
        comentarios
    })

    pelicula.save()
        .then(() => {
            codigoOK(res, "La pelicula se agrego con exito")
        })
        .catch((error) => {
            error500(res, error)
        })
}


module.exports.verPeliculas = (req, res) => {
    const filtro = req.query.nombre
    const filtroAplicar = {
        ...(filtro && ({ nombre: filtro }))
    }


    Pelicula.find(filtroAplicar)
        .then(data => {
            if (data != undefined) codigoOK(res, data)
            else error404(res,"No hay peliculas con ese nombre")// array vacio
        })
        .catch(error => error500(error))

}


module.exports.filtrarPorId = (req, res) => {
    const buscar = Pelicula.buscarPorId(req.params.id);

    buscar
        .then(resultado => {
            if (resultado != undefined) return codigoOK(res, resultado);
            error404(res, "No hay peliculas con esa id")
        })
        .catch(error => {
            error500(res, error)
        })
}


module.exports.eliminarPelicula = (req, res) => {

    const eliminar = Pelicula.deleteById(req.params.id)

    eliminar
        .then(resultado => {
            if (resultado.deletedCount == 0) error404(res, "No hay peliculas con esa id para borrar")
            else codigoOK(res, resultado.deletedCount)
        })
        .catch(error => {
            error500(res, error)
        })
}


module.exports.modificarPelicula = (req, res) => {
    Pelicula.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })//hacer patch
        .then(data => {
            if (data == undefined) error404(res, "No hay peliculas con esa id para modificar")
            else codigoOk(res, data)
        })
        .catch(error => error500(res, error))
}


module.exports.agregarComentario = (req, res) => {
    Pelicula.findOneAndUpdate({ _id: req.params.id }, { $push: { comentarios: req.body.comentarios } }, { new: true })
        .then(data => {
            if (data == undefined) error404(res, "No hay peliculas con esa id para agregar comentarios")
            else codigoOK(res, data)
        })
        .catch(error => error500(res, error))
}