const { Schema } = require("mongoose")

const ElencoSchema = new Schema({
    nombreReal: String,
    personaje: String
},{_id: false});

module.exports = ElencoSchema;