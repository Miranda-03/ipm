const { createToken } = require("../utils/tokenUtils");
const { codigoOK, error500, error401 } = require("../utils/utils");

module.exports.validateLogin = (req, res, next) => {


    if((req.body.user == "alumno" || req.body.user == "admin") && req.body.pass == "alumnoipm")
    {
        req.currentUserData = {
            user: req.body.user,
            pass: req.body.pass
        }
        next()
    }
    else{
        error401(res,"El usuario o la contraseña son incorrectos")
    }
}

module.exports.createSessionToken = (req, res, next) => {
    

    createToken(req.currentUserData) //hardcodeado
    .then(token => {
        res.userToken = token
        next()
    })
    .catch(error => {
        
        error500(res, "ocurrio un error inesperado")
    })
}

module.exports.sendLoginResponse = (req,res) => {
    const token = res.userToken

    const options = {
        maxAge: 5000 * 1000
    }

    res.cookie("token",token,{...options, httpOnly : true})
    res.cookie("isLogged",true,options)

    codigoOK(res,token)
}