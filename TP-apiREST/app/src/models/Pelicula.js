const mongoose = require("mongoose")
const ElencoSchema = require("./schemas/ElencoSchema")
const { Schema } = mongoose;

const PeliculaSchema = new Schema({
    _id: Number,
    nombre: String,
    anioEstreno: Number,
    directores: [String],
    elenco: [ElencoSchema],
    comentarios: [String]
},)

PeliculaSchema.statics.buscarPorId = function (id) {
    return this.findOne({_id: id})
}


PeliculaSchema.statics.deleteById = function (id) {
    return this.deleteOne({_id:id})
}

module.exports = mongoose.model("Pelicula", PeliculaSchema)