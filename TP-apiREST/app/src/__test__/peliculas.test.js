const app = require("../../app");
const { connectTestDB, disconnectTestDB } = require("../utils/testUtils");
const request = require("supertest")(app);
let token = "";
const Pelicula = require("../models/Pelicula");

beforeAll(async () => {
    await connectTestDB("peliculasTest2")
    await Pelicula.deleteById(111)

    const obtenerToken = await request.post("/auth/login")
    token = obtenerToken.body.message;
})

describe("Testing peliculas", () =>{

    describe("Post pelicula", () => {
        test("It should be successful", async () => {

            const createPelicula = {
    
                "_id": 111,
                "nombre": "kfkelmslkfmle",
                "anioEstreno": 58748,
                "directores": [
                    "fesfesf",
                    "gesgsegse"
                ],
                "elenco": [
                    {
                        "nombreReal": "fesfsef",
                        "personaje": "chancfesfesefho"
                    }
                ],
                "comentarios": [
                    "Es buenisima la mejor pelicula de la historia AMIGO WTFFF"
                ]
    
            };
    
    
            const response = await request.post("/peliculas/").set('Authorization', `Bearer ${token}`).send(createPelicula)
    
            const pelicula = await Pelicula.findOne({"_id": 111 }).lean();
    
            expect(response.statusCode).toBe(200)
            expect(pelicula).toMatchObject(createPelicula);
    
        })
    
        test("it should be wrong", async () => {
            
            const createPelicula = {
    
                "_id": 111,
                "nombre": "kfkelmslkfmle",
                "anioEstreno": 58748,
                "directores": [
                    "fesfesf",
                    "gesgsegse"
                ],
                "elenco": [
                    {
                        "nombreReal": "fesfsef",
                        "personaje": "chancfesfesefho"
                    }
                ]
    
            };
    
            const response = await request.post("/peliculas").set('Authorization', `Bearer ${token}`).send(createPelicula)
    
            expect(response.statusCode).toBe(400)
    
        })
    })

    describe("Get pelicula", () => {

        test("With status code 200", async () => {
       
            const response = await request.get("/peliculas/").set('Authorization', `Bearer ${token}`)
            expect(response.statusCode).toBe(200)
    
        })

    })

    describe("Get (specific) pelicula", () => {

        test("With code 200", async () => {

            const response = await request.get("/peliculas/111" ).set('Authorization', `Bearer ${token}`)
    
            expect(response.statusCode).toBe(200)
    
        })
    
        test("With code 404", async () => {
    
            const response = await request.get("/peliculas/223").set('Authorization', `Bearer ${token}`)
            
            expect(response.statusCode).toBe(404)
    
        })
    })

    describe("Delete pelicula", () => {
        test("It should be with code 200", async () => {
        
                const deletePelicula = await request.delete("/peliculas/111").set('Authorization', `Bearer ${token}`);
        
                expect(deletePelicula.statusCode).toBe(200)
        
        
            });
    })

})


afterAll(async () => {
    await disconnectTestDB();
})