const { validationResult } = require('express-validator');
const { error400 } = require('./utils');

module.exports.verifyvalidation = (req, res, next) => {
    
    const errors = validationResult(req)

    if(errors.isEmpty())
        return next()
    
    error400(res,errors.array());

}