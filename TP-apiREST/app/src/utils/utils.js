const fs = require('fs');
const fsPromises = fs.promises;
const config = require("config")

module.exports.obtenerJSON = async() => {
    const data = await fsPromises.readFile(config.get("jsonUrl"),"utf-8");

    return data;
}

module.exports.escribirJSON = async(buffer) => {
    fsPromises.writeFile(config.get("jsonUrl"),JSON.stringify(buffer,null,"\t"));

    return buffer;
}

module.exports.codigoOK = (res,mensaje) => {
    return res.status(200).json({
        code: 10,
        message: mensaje
    })
}

module.exports.error403 = (res,mensajeError) => {
    return res.status(403).json({
        code: 19,
        message: mensajeError
    })
}

module.exports.error404 = (res,mensajeError) => {
    return res.status(404).json({
        code: 20,
        message: mensajeError
    })
}

module.exports.error401 = (res,mensajeError) => {
    return res.status(401).json({
        code: 21,
        message: mensajeError
    })
}

module.exports.error400 = (res,mensajeError) => {
    return res.status(400).json({
        code: 22,
        message: mensajeError
    })
}

module.exports.error500 = (res,mensajeError) => {
    return res.status(500).json({
        code: 23,
        message: mensajeError
    })
}