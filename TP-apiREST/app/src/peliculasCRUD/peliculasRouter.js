const express = require('express');
const RouterPeliculas = express.Router();
const { agregarPeliculaValidator, modificarPeliculaValidator, validarId, validarComentario } = require('./peliculaValidator');
const { verPeliculas, filtrarPorNombre, eliminarPelicula, agregarPelicula, agregarComentario, modificarPelicula, filtrarPorId } = require("./peliculasController");
const { jwtValidator} = require('../authCRUD/authValidator');
const { validateToken, readToken, AdminValidator } = require('../utils/tokenUtils');

//localhost:3000/peliculas?nombre=pablo
//localhost:3000/peliculas/
RouterPeliculas.route("/")
    .get(verPeliculas)
    .post(agregarPeliculaValidator, agregarPelicula);

//localhost:3000/peliculas/7
RouterPeliculas.route("/:id")
    .all(validarId)
    .delete(jwtValidator, readToken, AdminValidator , eliminarPelicula)
    .patch(modificarPeliculaValidator, modificarPelicula)
    .get(filtrarPorId);

//localhost:3000/peliculas/7/comentarios
RouterPeliculas.post("/:id/comentarios", jwtValidator, readToken, validarComentario, agregarComentario);


module.exports = RouterPeliculas;
