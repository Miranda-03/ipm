const { body } = require("express-validator")
const { verifyvalidation } = require("../utils/validatorUtils")

module.exports.agregarPeliculaValidator = [
    body("nombre").isString().withMessage("El nombre debe estar y debe ser un string"),
    body("anioEstreno").isNumeric().withMessage("El año debe estar y debe ser numerico"),
    body("directores").isArray().withMessage("El campo directores debe estar y debe ser un array"),
    body("elenco").isArray().withMessage("El elenco debe estar y debe ser un array"),
    body("comentarios").isArray().withMessage("El campo comentarios debe estar y debe ser un array"),
    verifyvalidation
]

module.exports.modificarPeliculaValidator = [
    body("nombre").optional().isString().withMessage("El nombre debe ser un string"),
    body("anioEstreno").optional().isNumeric().withMessage("El año debe ser numerico"),
    body("directores").optional().isArray().withMessage("El campo directores debe ser un array"),
    body("elenco").optional().isArray().withMessage("El elenco debe ser un array"),
    body("comentarios").optional().isArray().withMessage("El campo comentarios debe ser un array"),
    verifyvalidation
]

module.exports.validarId = [
    //body("_id").isNumeric().withMessage("La id debe ser un ObjectID"),
    verifyvalidation
]

module.exports.validarComentario = [
    body("comentarios").isString().isLength({min : 1, max : 100}).withMessage("El comentario debe ser un string y tener entre 1 y 100 caracteres"),
    verifyvalidation
]