#TP API REST implementada con NodeJS

El TP consiste en desarrollar una API RESTful implementada en NodeJS utilizando ExpressJS
como framework.

##Requerimientos

Una importante empresa de cine está pensando en cambiar su sistema de catálogo de
películas. Como tienen un presupuesto acotado van a arrancar por hacer la API primero, para
este trabajo nos contactaron a nosotros y nos brindaron la siguiente información:
El catálogo de películas que quieren desarrollar es relativamente sencillo para comenzar.

###De las películas tenemos los siguientes datos:

● Nombre
● Año de estreno
● Director/es
● Elenco
● Comentarios

###Puntualmente nos pidieron que la API permita:

● Crear una nueva película para agregarla al catálogo.
● Obtener el listado de películas permitiendo filtrar por nombre.
● Eliminar una película.
● Modificar una película.
● Obtener una película en particular.
● Agregar comentarios a una película.

Por el momento están viendo qué proveedor de DB van a usar así que nos pidieron que los
datos que maneje la API los almacenemos en el file system. Para esto debemos definir cómo
vamos a almacenar los datos, pero nuestro Tech Lead nos dijo que nos conviene tener todo en
formato JSON sin mayor detalle así que los tipos de datos que usemos quedan bajo nuestro
criterio. Justificar la estructura de los datos elegida.
