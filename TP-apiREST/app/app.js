const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const peliculasRouter = require('./src/peliculasCRUD/peliculasRouter.js');
const AuthRouter = require('./src/authCRUD/authRouter.js');
const {validateToken, readToken } = require('./src/utils/tokenUtils');

const cors = require("cors");

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors({ credentials:true , origin: function (origin,callback) {
    if (origin == "http://localhost:3000") {
        callback(null,true)
    }
    else {
        callback(new Error("Not allowed by CORS"))
    }
} }))

app.use('/', indexRouter);
app.use('/auth', AuthRouter);
app.use('/peliculas', peliculasRouter);


module.exports = app;