const config = require("config");
const jwt = require("jsonwebtoken");
const { error500, error401, error403 } = require("./utils");
const { header } = require('express-validator');
let tokenGlobal;


module.exports.createToken = (body) => {
    return new Promise((resolve, reject) => {
        jwt.sign(body, config.get("authentication.jwtSecret"), { expiresIn: config.get("authentication.jwtExpiry") }, (error, token) => {
            if (error) return reject(error);

            resolve(token)
        });
    })
}

module.exports.readToken = (req, res, next) => {
    const authorizationToken = req.header.authorization ? req.header.authorization.split(':').split(' ')[1] : undefined
    const cookieToken = req.cookies.token

    const token = authorizationToken || cookieToken
    tokenGlobal = token
    if (!token)
        error401(res, "No estas logueado")

    verifyToken(token)
        .then((tokenData) => {
            next();
        })
        .catch((error) => {
            error401(res, "No estas logueado")
        })

}

const verifyToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, config.get("authentication.jwtSecret"), (error, tokenData) => {
            if (error) return reject(error)
            resolve(tokenData)
        })
    })
}

module.exports.AdminValidator = (req, res, next) => {

    
    verifyToken(tokenGlobal)
        .then((tokenData) => {
            const tokenAdmin = tokenData;
            if (tokenAdmin.user == "admin") return next()

            error403(res, "Debe ser admin");
        })
        .catch((error) => {
            error401(res, "No estas logueado")
        })



}