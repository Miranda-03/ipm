const mongoose = require("mongoose")
const config = require("config")

const uri = config.get("mongodbUri");

module.exports.connectToDB = function (dbname = "", params = {}) {
    return mongoose.connect(uri + dbname, params)
}

module.exports.disconnectToDB = function () {
    mongoose.disconnect();
}

