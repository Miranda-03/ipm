const express = require("express");
const AuthRouter = express.Router();
const { validateLogin, createSessionToken, sendLoginResponse } = require("./authController.js");


AuthRouter.post("/login", validateLogin, createSessionToken, sendLoginResponse);


module.exports = AuthRouter;