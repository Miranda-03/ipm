const { connectToDB, disconnectToDB } = require("./dbUtils")

module.exports.connectTestDB = (dbSuitName) => { connectToDB( dbSuitName, { autoIndex: false }) }

module.exports.disconnectTestDB = () => disconnectToDB();
